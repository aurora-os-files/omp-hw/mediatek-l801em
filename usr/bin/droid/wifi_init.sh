#!/bin/sh

# Enable wifi via the interfaces that libhybris provides (uses vendor specific
# implementation, which notifies other vendor processes of wifi enablement).
/usr/bin/hybris_wifi_helper 1

# This device uses gen2 of the wmtWifi WLAN driver and needs to initialize all
# interfaces at least once, before functioning properly (avoids crashes when
# connecting to certain WLAN networks).

WIFI_NODE=/dev/wmtWifi

# Initialize the ap0 interface.
echo "A" > $WIFI_NODE
# Initialize the p2p0 interface.
echo "P" > $WIFI_NODE

# (Re-)Initialize the wlan0 interface.
# After this the device can connect to WLAN networks without crashes and
# connman can configure it how it wants to.
echo "S" > $WIFI_NODE

