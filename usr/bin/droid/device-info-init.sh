#!/bin/sh

while [ "$(/usr/bin/getprop service.nvram_init)" != "Ready" ]; do
    sleep 1
done

hexdump -s 0 -n 6 -ve '/1 "%02X:"' /nvdata/APCFG/APRDEB/BT_Addr | sed "s/:$//g" > /var/lib/bluetooth/board-address

calibration_flag=$(printf "%d" 0x$(hexdump -s 1020 -n 4 -ve '/1 "%02X"' /dev/block/platform/mtk-msdc.0/by-name/proinfo |tac -rs ..))

# avoid removing whitespaces from the calibration string
IFS=$'\n'

calibration_string=
while [ -z "$calibration_string" ]; do
    sleep 1
    calibration_string=$(/usr/bin/getprop gsm.serial)
done

# filter unallowed characters which would break the ini format
calibration_string=$(echo $calibration_string | sed "s/\"/\\\"/g")

echo "[CalibrationInfo]" > /run/csd/settings.d/30-hw-settings-device-info.ini
echo "Flag=$calibration_flag" >> /run/csd/settings.d/30-hw-settings-device-info.ini
echo "String=\"$calibration_string\"" >> /run/csd/settings.d/30-hw-settings-device-info.ini
echo >> /run/csd/settings.d/30-hw-settings-device-info.ini

# Remove whitespaces and store serial number for settings app.
mkdir -p /run/config

echo "$calibration_string" | awk '{ print $1 }' >> /run/config/serial
echo >> serial
