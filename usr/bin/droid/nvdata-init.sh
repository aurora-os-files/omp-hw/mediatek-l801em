#!/bin/sh

set -e

NVDATA_PART=/dev/block/platform/mtk-msdc.0/by-name/nvdata
if [ ! -e $NVDATA_PART ]; then
    NVDATA_PART=/dev/mmcblk0p19
fi

# if it doesn't contain an ext4 system we need to format it
if ! blkid -t TYPE=ext4 $NVDATA_PART; then
    mkfs.ext4 $NVDATA_PART
fi

