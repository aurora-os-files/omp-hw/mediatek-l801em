
= FLASHING =

Before starting flashing on any host turn off the device. After this follow the
instructions given for your host PC operating system.

By this point of time you should already have the .tar.bz2 file that contains
the image as this flashing instructions file that you are reading at the moment
is inside that .tar.bz2 file. As a general note the flashing can take a long
time (>10 minutes) and it flashes image with similar name multiple times in the
end which is expected behaviour.


== LINUX ==

Open terminal application and go to the folder where the image is extracted.

Next:
* Hold device so that USB connector is up.

INOI T8 and DIGMA CITI 8527 4G
* NOTE: If you have Android on your device follow "Flashing Android INOI T8 or
  DIGMA CITI 8527 4G tablet" section instructions.
* Connect device to computer with USB-cable while holding volume down button.
* Wait until boot mode selection is show in display.
* Change between the boot modes by pressing volume down button.
* Select fastboot mode by pressing volume up button.

INOI T10
* Connect device to computer with USB-cable while holding volume up button.
* Wait until boot mode selection is show in display.
* Change between the boot modes by pressing volume up button.
* Select fastboot mode by pressing volume down button.

* Next start flashing script by entering following command:

  bash ./flash.sh

* Enter your password if requested to gain root access for flashing the device
* Once flashing is completed you will see text: 

  "Flashing completed. Detact usb cable, press and hold the powerkey to reboot."

* After following the guidance from script device should boot up to new Sailfish OS

NOTE: If flashing does not succeed, you might have missing fastboot binary or
it is too old. Many distros include andoid-tools package, but that might not
be new enough to support device flashing.

Installation commands for some linux distributions:
* Ubuntu: sudo apt-get install android-tools-fastboot

If you want to compile fastboot binary for your distro you can compile version
5.1.1 release 38 or newer from:
https://github.com/mer-qa/qa-droid-tools


== Flashing Android INOI T8 or DIGMA CITI 8527 4G ==

* Install MediaTek SP Flash Tool v5.1728.00 on your PC.
* Download Sailfish_Mobile_OS_RUS-OMP_RU l801em tar.bz2 image to your PC and extract it.
* Open MediateTek SP Flash Tool and select Download tab.
* Make sure that "Download only" flashing mode is selected.
* Click "choose" from "Scatter-loading File" field.
* Select MT6735_Sailfish_OS_scatter.txt for INOI T8 or digma-citi-8527-4g-scatter.txt
  for DIGMA CITI 8527 4G and click open.
* As a final step click "Download" button and connect powered off tablet to usb cable.

