#!/bin/bash

# Contact: Matti Kosola <matti.kosola@jollamobile.com>
#
#
# Copyright (c) 2017, Jolla Ltd.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
# * Neither the name of the <organization> nor the
# names of its contributors may be used to endorse or promote products
# derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

set -e

VALID_PRODUCT="ELINK8735_TB_N"

function check_fastboot {
  FASTBOOT_BIN_NAME=$1
  if [ -f "$FASTBOOT_BIN_NAME" ]; then
    chmod 755 $FASTBOOT_BIN_NAME
    # Ensure that the binary that is found can be executed fine
    if ./$FASTBOOT_BIN_NAME help &>/dev/null; then
      FASTBOOT_BIN_PATH="./"
      return 0
    fi
  fi
  return 1
}


# Do not need root for fastboot on Mac OS X
if [ "$(uname)" != "Darwin" -a $(id -u) -ne 0 ]; then
  exec sudo -E bash $0
fi

UNAME=$(uname)
OS_VERSION=

case $UNAME in
  Linux)
    echo "Detected Linux"
    ;;
  Darwin)
    IFS='.' read -r major minor patch <<< $(sw_vers -productVersion)
    OS_VERSION=$major-$minor
    echo "Detected Mac OS X - Version: $OS_VERSION"
    ;;
  *)
    echo "Failed to detect operating system!"
    exit 1
    ;;
esac

FASTBOOT_BIN_PATH=
FASTBOOT_BIN_NAME=

if ! check_fastboot "fastboot-$UNAME-$OS_VERSION" ; then
  if ! check_fastboot "fastboot-$UNAME"; then
    # In case we didn't provide functional fastboot binary to the system
    # lets check that one is found from the system.
    if ! which fastboot &>/dev/null; then
      echo "No 'fastboot' found in \$PATH. To install, use:"
      echo ""
      echo "    Debian/Ubuntu/.deb distros:  apt-get install android-tools-fastboot"
      echo "    Fedora:  yum install android-tools"
      echo "    OS X:    brew install android-sdk"
      echo ""
      exit 1
    else
      FASTBOOT_BIN_NAME=fastboot
    fi
  fi
fi

echo "Searching for device to flash.."
IFS=$'\n'
FASTBOOTCMD_NO_DEVICE="${FASTBOOT_BIN_PATH}${FASTBOOT_BIN_NAME}"

FASTBOOT_DEVICES=$($FASTBOOTCMD_NO_DEVICE devices | awk '{ print $1 }')

if [ -z "$FASTBOOT_DEVICES" ]; then
  echo "No device that can be flashed found. Please connect your device in fastboot mode before running this script."
  exit 1
fi

TARGET_SERIALNO=
count=0
for SERIALNO in $FASTBOOT_DEVICES; do
  PRODUCT=$($FASTBOOTCMD_NO_DEVICE -s $SERIALNO getvar product 2>&1 | grep "product:" | awk '{ print $2 }')

  if [ ! -z "$(echo $PRODUCT | grep -o "$VALID_PRODUCT")" ]; then
    TARGET_SERIALNO="$SERIALNO $TARGET_SERIALNO"
    ((++count))
  fi
done

echo "Found $count device(s): $TARGET_SERIALNO"

if [ $count -ne 1 ]; then
  echo "Incorrect number of devices connected. Make sure there is exactly one device connected in fastboot mode."
  exit 1
fi

FASTBOOTCMD="${FASTBOOT_BIN_PATH}${FASTBOOT_BIN_NAME} -s $TARGET_SERIALNO $FASTBOOTEXTRAOPTS"

FLASHCMD="$FASTBOOTCMD flash"

if [ -z ${BINARY_PATH} ]; then
  BINARY_PATH=./
fi

if [ -z ${SAILFISH_IMAGE_PATH} ]; then
  SAILFISH_IMAGE_PATH=./
fi

IMAGES=(
"preloader ${SAILFISH_IMAGE_PATH}preloader_elink8735_tb_n.bin"
"lk ${SAILFISH_IMAGE_PATH}lk.bin"
"boot ${SAILFISH_IMAGE_PATH}hybris-boot.img"
"recovery ${SAILFISH_IMAGE_PATH}hybris-recovery.img"
"logo ${SAILFISH_IMAGE_PATH}logo.bin"
"ITEMS ${SAILFISH_IMAGE_PATH}items.ini"
"secro ${SAILFISH_IMAGE_PATH}secro.img"
"tee1 ${SAILFISH_IMAGE_PATH}trustzone.bin"
"tee2 ${SAILFISH_IMAGE_PATH}trustzone.bin"
"cache ${SAILFISH_IMAGE_PATH}cache.img"
"userdata ${SAILFISH_IMAGE_PATH}sailfish.img001"
"system ${SAILFISH_IMAGE_PATH}fimage.img001"
)

IFS=' '
for IMAGE in "${IMAGES[@]}"; do
  read partition ifile <<< $IMAGE
  if [ ! -e ${ifile} ]; then
    echo "Image binary missing: ${ifile}."
    exit 1
  fi
done

for IMAGE in "${IMAGES[@]}"; do
  read partition ifile <<< $IMAGE
  echo "Flashing $partition partition.."
  $FLASHCMD $partition $ifile
done

echo "Flashing completed. Detach usb cable, press and hold the powerkey to reboot."
