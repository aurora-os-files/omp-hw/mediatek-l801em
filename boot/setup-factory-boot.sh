#!/bin/bash

#set -e

# This file is executed during image build after the factory snapshot of the image
# is done. This is to make sure that the changes made here are not applied to 
# the actual image that is recovered with factory reset.

if [ -z ${HOME_PATH} ] || [ ! -e ${HOME_PATH} ]; then
  echo "HOME_PATH not valid."
  exit 1
fi

if [ -z ${ROOT_PATH} ] || [ ! -e ${ROOT_PATH} ]; then
  echo "ROOT_PATH not valid."
  exit 1
fi

# Figure out correct default UID
DEF_UID=$(grep "^UID_MIN" $ROOT_PATH/etc/login.defs |  tr -s " " | cut -d " " -f2)

# Touch files to skip startup wizard and tutorial
TOUCH_FILES=".jolla-startupwizard-done .jolla-startupwizard-sfos2-tutorial .jolla-startupwizard-usersession-done"

for TF in $TOUCH_FILES; do
  touch $HOME_PATH/$TF
  chown $DEF_UID:$DEF_UID $HOME_PATH/$TF
done

# Add automatic startup for csd tool
mkdir -p $HOME_PATH/.config/systemd/user/post-user-session.target.wants/
ln -s /usr/lib/systemd/user/jolla-csd@.service $HOME_PATH/.config/systemd/user/post-user-session.target.wants/jolla-csd@factoryStartup.service
chown $DEF_UID:$DEF_UID -R $HOME_PATH/.config

# Another thing to skip tutorial
# NOTE: This works as long as dconf-update is executed on first boot, see:
# https://git.merproject.org/mer-core/dconf/blob/master/rpm/dconf-update#L5
cat > $ROOT_PATH/etc/dconf/db/nemo.d/factory.txt <<EOF
[apps/jolla-startupwizard]
reached_tutorial=true
[sailfish/text_input]
active_layout='en.qml'
enabled_layouts=['en.qml']
EOF

# Do not hide any USB mode
sed -i 's/hide=.*/hide=/g' $ROOT_PATH/etc/usb-moded/usb-moded.ini
# Set usb to diag mode by default
sed -i 's/mode=.*/mode=diag_mode/g' $ROOT_PATH/etc/usb-moded/usb-moded.ini
# Enable adb root
sed -i 's/ro.secure=.*/ro.secure=0/g' $ROOT_PATH/default.prop

if [ "$1" == "--english" ]; then
  # Set language to English
  echo "LANG=en_GB.utf8" > $ROOT_PATH/var/lib/environment/nemo/locale.conf
else
  # Set language to Chinese
  echo "LANG=zh_CN.utf8" > $ROOT_PATH/var/lib/environment/nemo/locale.conf
fi

# Set audio volume to max
sed -i -e 's/\(.*x-maemo \).*/\10/' -e 's/\(.*phone \).*/\10 0/' $ROOT_PATH/etc/pulse/x-maemo-route.table
sed -i -e 's/\(.*\) .*/\1 0/' $ROOT_PATH/etc/pulse/x-maemo-stream-restore.table
sed -i -e '/^x-nemo.mainvolume.high-volume-step/d' $ROOT_PATH/var/lib/nemo-pulseaudio-parameters/modes/lineout/mainvolume

# Disable ALS, set brightness to 100 and set Dim timeout to 10min
cat > $ROOT_PATH/etc/mce/90-factory-config.conf <<EOF
/system/osso/dsm/display/als_enabled=false
/system/osso/dsm/display/display_dim_timeout=600
/system/osso/dsm/display/display_brightness=100
EOF

